package com.sda.javarzw3.projektgrupowy.rest.repository;

import com.sda.javarzw3.projektgrupowy.rest.model.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "employees", path = "/api/employees")
public interface EmployeeRepository extends MongoRepository<Employee, String> {
}
