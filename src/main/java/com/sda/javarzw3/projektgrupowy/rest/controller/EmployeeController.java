package com.sda.javarzw3.projektgrupowy.rest.controller;

import com.sda.javarzw3.projektgrupowy.rest.model.Employee;
import com.sda.javarzw3.projektgrupowy.rest.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController()
@RequestMapping(path = "/api/employees")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping(path = "/{id}")
    public ResponseEntity<Employee> get(@PathVariable(name = "id") String id) {
        return ResponseEntity.ok(this.employeeService.get(id).orElse(null));
    }

    @GetMapping
    public ResponseEntity<List<Employee>> getAll() {
        return ResponseEntity.ok(this.employeeService.getAll());
    }

    @PostMapping
    public ResponseEntity<Employee> add(@RequestBody Employee employee) {
        return ResponseEntity.ok(this.employeeService.add(employee));
    }
}
