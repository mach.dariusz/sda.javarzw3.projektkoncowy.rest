package com.sda.javarzw3.projektgrupowy.rest.service;

import com.sda.javarzw3.projektgrupowy.rest.model.Employee;
import com.sda.javarzw3.projektgrupowy.rest.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public Employee add(Employee employee) {
        return this.employeeRepository.save(employee);
    }

    public List<Employee> getAll() {
        return this.employeeRepository.findAll();
    }

    public Optional<Employee> get(String id) {
        return this.employeeRepository.findById(id);
    }
}
