# REST project

Description here

## How to run this project

```
mvn clean package
java -jar target/rest-0.0.1-SNAPSHOT.jar
```

## Example of code block

```
class A {
    private int i;
}
```

# Example of inline code

Example of `code blok` here
